import tensorflow.compat.v1 as tf
from tensorflow.compat.v1.keras.applications import VGG16
from tensorflow.compat.v1.keras.models import Model
from tensorflow.compat.v1.keras.layers import Conv2D, Dropout, Flatten, MaxPooling2D, Dense

def construct_model(params, metrics):
    print(f'\nConstructing model with {params.EXTRACTION_LAYER_NUMBER} fixed pre-trained layers from VGG16 and trainable convolutional tail.\n')

    # Get first 5 block of pretrained VGG16 model
    vgg16_head = VGG16(weights='imagenet', include_top=False, input_shape=params.INPUT_SHAPE)
    # vgg16_head.summary()
    # Associate layers with layernames for easier referencing
    layer_dict = dict([(layer.name, layer) for layer in vgg16_head.layers])
    # The layer to be used for transfer learning
    x = layer_dict[f'block{params.EXTRACTION_LAYER_NUMBER}_pool'].output

    # Stack new convolutional network on top
    x = Conv2D(filters=64, kernel_size=(3, 3), activation='relu')(x)
    x = MaxPooling2D(pool_size=(2, 2))(x)
    x = Flatten()(x)
    x = Dense(256, activation='relu')(x)
    # x = Dropout(0.5)(x)
    x = Dense(params.N_CLASSES, activation='sigmoid')(x)

    # Create new model
    model = Model(inputs=vgg16_head.input, outputs=x)

    # Freeze pre-trained layers
    for layer in model.layers:
        layer.trainable = False
        if layer.name == f'block{params.EXTRACTION_LAYER_NUMBER}_pool':
            break
    # model.summary()

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=metrics)

    return model
