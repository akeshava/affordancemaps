"""
Code ideas from https://github.com/Newmu/dcgan and tensorflow mnist dataset reader
"""
import numpy as np
import scipy.misc as misc
from skimage.transform import rescale
import scipy.io as io
import TensorflowUtils as utils
import tensorflow as tf

class BatchDatset:
    files = []
    images = []
    annotations = []
    image_options = {}
    batch_offset = 0
    epochs_completed = 0


    def __init__(self, records_list, image_options={}):
        """
        Intialize a generic file reader with batching for list of files
        :param records_list: list of file records to read -
        sample record: {'image': f, 'annotation': annotation_file, 'filename': filename}
        :param image_options: A dictionary of options for modifying the output image
        Available options:
        resize = True/ False
        resize_size = #size of output image - does bilinear resize
        color=True/False
        """
        self.files = records_list
        self.image_options = image_options
        self._read_images()
        # self.validation = validation

    def _read_images(self):
        self.__channels = True
        self.images = np.array([self._transform(filename) for filename in self.files[0]])
        self.__channels = False
        self.annotations = np.array([self._transform_annotation(filename) for filename in self.files[1]])
        # im_reader = tf.WholeFileReader()
        # im_key,im_raw =im_reader.read(self.files)
        # # ann_reader = tf.WholeFileReader()
        # # ann_key,ann_raw =ann_reader.read(self.files)
        # # image = self._transform(tf.image.decode_jpeg(im_raw))
        # image = self._transform(tf.image.decode_jpeg(im_raw))
        #
        #
        # print(image.shape)
        # (f_image,f_ann) = self.files
        # print(f_image.shape)




    def _transform(self, filename):
        image = misc.imread(filename)
        padding = np.zeros([224,224, 3])

        if self.__channels and len(image.shape) < 3:  # make sure images are of shape(h,w,3)
            image = np.array([image for i in range(3)])

        # if self.image_options.get("resize") and self.image_options["resize"]:
        #     image = int(self.image_options["resize_size"])
        #     image = misc.imresize(image,
        #                                  [256, 256], interp='nearest')


        image = self._add_padding(image, padding)


        return image

    def _transform_annotation(self,filename):

        label = io.loadmat(filename)
        padding = np.zeros([224,224,1])
        resize_image = label['gt_label']

        resize_image = self._add_padding(np.expand_dims(resize_image,axis = 2), padding)
        if np.count_nonzero(resize_image) == 0:
            print('bad annotations', filename)
        if self.image_options.get("resize") and self.image_options["resize"]:
            resize_size = int(self.image_options["resize_size"])
            resize_image = misc.imresize(resize_image,
                                         [256, 256], interp='nearest')


        return resize_image


    def _add_padding(self, img, padding):
        x,y,ch = img.shape
        sz = padding.shape[0]
        padx = (sz-x)/2
        pady = (sz-y)/2
        if x >= sz and y>=sz:
            padding = img[:sz,:sz,:]
        elif x >= sz and y<sz:
            padding[:,pady:pady+y,:] = img[:sz,:,:]
        elif x<sz and y<sz:
            padding[padx:padx+x,pady:pady+y,:] = img
        elif x<sz and y>=sz:
            padding[padx:padx+x,:,:] = img[:,:sz,:]
        # print('padding')
        # print(padding.shape)
        return padding

    def _data_preprocess(self,img, scale, annotation):
        img = rescale(img,scale)
        if np.count_nonzero(img) == 0 and annotation:
            print('bad annotations')
        x,y,ch = img.shape
        out = np.zeros([224,224,ch])
        out[:x,:y,:] = img
        if not annotation:
            out = out - out.mean() / out.std()
        return out



    def get_records(self):
        return self.images, self.annotations

    def reset_batch_offset(self, offset=0):
        self.batch_offset = offset

    def next_batch(self):
        start = self.batch_offset
        # self.batch_offset += batch_size
        # if self.batch_offset > self.images.shape[0]:
            # Finished epoch
            # self.epochs_completed += 1
            # print("****************** Epochs completed: " + str(self.epochs_completed) + "******************")
            # Shuffle the data
        # perm = np.arange(self.images.shape[0])
        # np.random.shuffle(perm)
        # self.images = self.images[perm]
        # self.annotations = self.annotations[perm]
        # Start next epoch
        start = 0
        # self.batch_offset = batch_size
        end = self.batch_offset
        scale = np.random.uniform(low=0.2, high=0.9)
        #random crop
        # x0=np.random.randint(0,32)
        # y0=np.random.randint(0,32)
        temp_img = np.array([self._data_preprocess(img,1., False) for img in self.images])
        temp_lab = np.array([self._data_preprocess(img,1., True) for img in self.annotations])
        # self.images = temp_img
        # self.annotations = temp_lab


        return temp_img, temp_lab



    def get_random_batch(self, batch_size):
        perm = np.arange(self.images.shape[0])
        np.random.shuffle(perm)
        indexes = np.random.randint(0, self.images.shape[0], size=[batch_size]).tolist()
        # x0=np.random.randint(60,70)
        # y0=np.random.randint(60,70)
        # temp_img = np.array([self._data_preprocess(img,x0,y0,224,True) for img in self.images[indexes]])
        # temp_lab = np.array([self._data_preprocess(img,x0,y0,224,True) for img in self.annotations[indexes]])
        # return self.images[indexes],self.annotations[indexes]
        print('rand batch' + str(self.images[indexes].shape))
        return self.images[indexes], self.annotations[indexes]

# if __name__ == "__main__":
#     data_dir = './2dseg_data/'
#     import affordance_parsing as aff
#     rec = aff.read_dataset(data_dir)
#     image_options = {'resize': False, 'resize_size': 0}
#     bd = BatchDatset(rec['training'][:5], image_options)
