##############################################################################
# This module computes a data set from the affordance dataset:
#
# Affordance Detection of Tool Parts from Geometric Features,
# Austin Myers, Ching L. Teo, Cornelia Fermüller, Yiannis Aloimonos.
# International Conference on Robotics and Automation (ICRA). 2015.
#
# The dataset can be found online at
# http://users.umiacs.umd.edu/~amyers/part-affordance-dataset/
#
# The data set this module computes consists of two numpy arrays representing
# the inputs and corresponding labels to train a neural network on the data.
# The input array is computed from the RGB .jpeg files found in the data set,
# while the labels only contain the unique and not pixel-wise labels
#
# @author Pascal Schröder
# @username verrannt
# @date Oct/28/2019
##############################################################################

import numpy as np
from scipy.io import loadmat
from PIL import Image
import os, sys

class AffordanceDataset():
    '''
    Class that reads the files in the 'tools' folder of the affordance dataset
    and return a train and test images and labels as numpy arrays
    '''

    def __init__(self, path_to_dataset):
        self.PATH = path_to_dataset

    def read_as_array(self, filename):
        '''
        Read a .jpg file as numpy array
        @params:
            path_to_image : string with directory
        @return:
            Numpy array of the original image
        '''
        with open(filename, 'rb') as f:
            im = np.array(Image.open(f), dtype=np.float32)        
        return im

    def compress_labels(self, labels):
        '''
        Gets the 3-D input of pixel-wise labels as input and returns the
        unique elements without 0
        '''
        # Get unique values
        x = np.unique(labels)
        # Remove 0 label
        x = x[np.nonzero(x)]
        return x

    def create(self, verbose = 0):

        # Ensure that path ends with '/'
        if not self.PATH[-1] is '/':
            self.PATH+='/'
        print(f"The full path to the dataset is {self.PATH}")

        # Get names of all objects in the dataset directory
        object_names = sorted(os.listdir(self.PATH))
        # Get size of data set
        n = len(object_names)

        # Lists that hold
        self.inputs = []
        self.labels = []

        # For each object
        print("Computing data …")
        cnt = 0.1
        for i, object_name in enumerate(object_names):
            print(object_name)
            if i/n == cnt:
                print(f'{cnt*100} %')
                cnt+=0.1
            # Get the names of all instances that are recorded
            instances = [name.split('_')[2] for name in os.listdir(self.PATH + object_name)]
            instances = np.unique(sorted(instances))
            # For each instance
            for instance in instances:
                if verbose == 1: print('    '+instance)
                # Compute numpy array of the RGB image
                im = self.read_as_array(self.PATH+f'{object_name}/{object_name}_{instance}_rgb.jpg')
                # And a 1-D numpy array holding the compressed labels
                lbl = loadmat(self.PATH+f'{object_name}/{object_name}_{instance}_label.mat')['gt_label']
                lbl = self.compress_labels(lbl)
                # Append these to the inputs and labels arrays
                self.inputs.append(im)
                self.labels.append(lbl)
            print(f"Size of input list: {sys.getsizeof(self.inputs)}, Size of label list: {sys.getsizeof(self.labels)}")
            break

        return self.inputs, self.labels
