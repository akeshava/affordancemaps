################################################################################
#
# Main script to run the affordance classifier. New models can be initialized,
# or models can be loaded from storage. Supplies training on affordance dataset
# (given data is available locally), testing on validation data, and analysis
# of the given dataset.
#
# @author Pascal Schröder (verrannt)
# @date Feb/2020
#
################################################################################

import tensorflow.compat.v1 as tf
from tensorflow.compat.v1.keras.backend import set_session
import numpy as np
import cv2

import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

import os, sys, time
import random, importlib, argparse
from datetime import datetime

from utils.printi import printi
from utils.hyperparametermanager import HyperParameterManager
import utils.customdatagen as datagen


def load_model(dir, name):
    """
    Load a pretrained model from storage. If the file could not be found under
    the specified path, catch OSError and exit the script.
    ---
    @params
    dir  : the directory where the model files reside
    name : the name of the specific model file
    ---
    @returns
    the loaded model.
    """
    try:
        model = tf.keras.models.load_model(f'{dir}{name}.h5')
    except OSError:
        printi(f"Model '{name}' not found in '{dir}'. Please check again.", 3)
        sys.exit()
    return model

def build_new_model():
    """
    Build a new model for affordance classification.
    Imports a new affordance model with pretrained and frozen VGG16 head from
    modelzoo (check the code for details) and constructs it with precision
    and recall metrics for multiclass classification.
    ---
    @returns
    the compiled model
    """
    from modelzoo.pretrainedVGG16head import construct_model
    metrics = [tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
    model = construct_model(params, metrics)
    model.summary()
    return model

def save_model(model, dir, n_epochs, last_loss, save_name=None, history=None):
    """
    Saves a model with a name if save_name is given, otherwise uses current
    time, number of epochs and last validation loss for name. Also saves the
    history object that comes with the model as plain text fail to
    '<dir>/histories'.
    ---
    @params
    model     : the model to be saved
    history   : the history object yielded from training the model
    dir       : directory to save the model at
    n_epochs  : number of epochs the model was trained for
    save_name : a unique name to be given optionally
    ---
    @returns
    the complete save path for printing success messages
    """

    # Save model
    if not save_name:
        timestemp = datetime.now().strftime('%Y-%m-%d--%H:%M:%S')
        save_name = f'affordance-model--{timestemp}--eps:{n_epochs}--val_loss:{last_loss}'
    complete_save_path = f'{dir}{save_name}.h5'
    model.save(complete_save_path)

    # Save history
    if history:
        import json
        history_dir = f'{dir}histories/'
        if not os.path.exists(history_dir):
            os.makedirs(history_dir)
        with open(f'{history_dir}{save_name}.history', 'w') as file:
            file.write(json.dumps(str(history.history)))

    return save_name

def plot_history(history, also_print=False, save_to_storage=False):
    """
    Plot the history of the training process with PyPlot.
    ---
    @params
    history         : the history object to plot
    also_print      : if True, print the history to CLI
    save_to_storage : whether to save the plot to storage. Defaults to False.
      Otherwise, value is used as complete path to store the plot at.
    ---
    @returns
    None
    """

    n_epochs = len(history.history['loss'])

    if also_print:
        print(f'Train history: {history.history}')

    fig = plt.figure(figsize=(8,10))
    fig.suptitle('Training history', fontsize=16)

    ax1 = fig.add_subplot(311)
    ax2 = fig.add_subplot(312)
    ax3 = fig.add_subplot(313)

    ax1.set_ylabel('Loss')
    ax1.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax1.plot(history.history['loss'], label='training set')
    ax1.plot(history.history['val_loss'], label='validation set')
    ax1.legend()

    ax2.set_ylabel('Precision')
    ax2.tick_params(axis='x', which='both', bottom=False, top=False, labelbottom=False)
    ax2.plot(history.history['precision'], label='training set')
    ax2.plot(history.history['val_precision'], label='validation set')

    ax3.set_ylabel('Recall')
    ax3.set_xlabel('Epoch')
    ax3.set_xticks(np.arange(n_epochs))
    ax3.plot(history.history['recall'], label='training set')
    ax3.plot(history.history['val_recall'], label='validation set')

    # plt.tight_layout()
    fig.subplots_adjust(left=0.14, right=0.96,
                        bottom=0.11, top=0.92,
                        wspace=0.2, hspace=0.07)
    if save_to_storage:
        plt.savefig(save_to_storage, dpi=100)
    plt.show()

def show_data_stats():
    """
    Compute and display descriptive statistics of the dataset, like size of
    the different classes and class balance metrics.
    ---
    @params
    None
    ---
    @returns
    None
    """
    # Count class occurences
    # … To ease computational load, the results from the following code
    #   are hard-coded in the subsequent else clause. Set to True to re-compute.
    if True:
        n = 0 # number of datapoints in both train and validation set
        train_occurences = np.zeros(7, int)
        for i in range(len(train_gen)):
            _, batchY = train_gen[i]
            for element in batchY:
                train_occurences = train_occurences + element
                print(train_occurences)
                n += 1
        test_occurences = np.zeros(7, int)
        for i in range(len(test_gen)):
            _, batchY = test_gen[i]
            for element in batchY:
                test_occurences = test_occurences + element
                n += 1
    else:
        n = 9920
        train_occurences = np.array([6321, 2054, 1394, 3218,  928,  785, 2192])
        test_occurences = np.array([1574,  531,  365,  796,  217,  184,  530])
    total_occurences = train_occurences + test_occurences

    print('Number of occurences of a particular class:\n')
    print(' Class\t\t', *(f'|\t{class_dict[i]}\t' for i in range(1,8)))
    print(16*'-', *('| '+13*'-' for _ in range(7)))
    print(' Trainset\t',*(f'|\t{oc}\t' for oc in train_occurences))
    print(' Testset\t',*(f'|\t{oc}\t' for oc in test_occurences))
    print(' Total\t\t',*(f'|\t{oc}\t' for oc in total_occurences))

    plt.subplot(121)
    plt.title('Absolute Occurences of Class Labels')
    p1 = plt.bar(np.arange(7), train_occurences)
    p2 = plt.bar(np.arange(7), test_occurences)
    plt.ylabel('No. of occurences')
    plt.xticks(np.arange(7), class_dict.values(), rotation=45)
    plt.yticks(np.arange(0, 8001, 1000))
    plt.legend((p1[0], p2[0]), ('Training Set', 'Test Set'))

    plt.subplot(122)
    plt.title('Relative Occurences of Class Labels')
    p3 = plt.bar(np.arange(7), train_occurences/max(train_occurences))
    p4 = plt.bar(np.arange(7), -test_occurences/max(test_occurences))
    plt.ylabel('No. of occurences')
    plt.xticks(np.arange(7), class_dict.values(), rotation=45)
    plt.legend((p3[0], p4[0]), ('Training Set', 'Test Set'))

    plt.show()

    n = sum(total_occurences)
    entropy = -1 * sum([total_occurences[i]/sum(total_occurences)
        *np.log(total_occurences[i]/sum(total_occurences))
        for i in range(7)])
    balance = entropy / np.log(7)
    print()
    print(f'Entropy-based balance of whole dataset:\t{balance:.4f}')
    print(f'Minmax-based balance of whole dataset:\t{min(total_occurences)/max(total_occurences):.4f}')

def test_model():
    # Compute multilabel confusion matrix across validation set
    from sklearn.metrics import multilabel_confusion_matrix
    batchX, gt = test_gen[0] # take inputs and labels from first batch
    pred = model.predict(batchX) # compute first batch prediction
    for i in range(1, len(test_gen)): # repeat for all remaining batches
        batchX, batchY = test_gen[i]
        gt = np.concatenate((gt, batchY), axis=0)
        pred = np.concatenate((pred, model.predict(batchX)), axis=0)
    binarizer = lambda x: 1 if x > 0.5 else 0
    pred = np.vectorize(binarizer)(pred)
    mcm = multilabel_confusion_matrix(gt, pred)

    # class_cnt = 1
    # for clas in mcm:
    #     print(f"Confusion Matrix for class '{class_dict[class_cnt]}'")
    #     for row in clas:
    #         print(row[0], row[1])
    #     class_cnt += 1

    # Plot confusion matrix for all classes
    fig, axes = plt.subplots(1, 8, figsize=(20,4))
    fig.suptitle('Confusion matrices for all classes', fontsize=16)
    cnt = 0
    axes[0].matshow([[0,0],[0,0]], cmap='Greys')
    axes[0].text(0, 0, 'TN', ha='center', va='center')
    axes[0].text(1, 0, 'FP', ha='center', va='center')
    axes[0].text(0, 1, 'FN', ha='center', va='center')
    axes[0].text(1, 1, 'TP', ha='center', va='center')
    axes[0].tick_params(axis='x', which='both', bottom=False, top=True)
    for ax in axes[1:]:
        ax.set_title(class_dict[cnt+1], fontsize=12)
        ax.matshow(mcm[cnt])
        ax.tick_params(axis='x', which='both', bottom=False, top=True)
        for (i, j), z in np.ndenumerate(mcm[cnt]):
            ax.text(j, i, z, ha='center', va='center')
        cnt += 1
    plt.show()

    # Compute precision and recall metrics per class
    print('\nPrecision and recall per class:\n')
    # print('Class:\t', *(f'\t{class_dict[i]}\t' for i in range(1,8)))
    print(' Class\t\t', *(f'|\t{class_dict[i]}\t' for i in range(1,8)))
    print(16*'-', *('| '+13*'-' for _ in range(7)))
    print(' Precision\t', *(f'|\t{mcm[i,1,1] / (mcm[i,1,1]+mcm[i,0,1]):.4f}\t' for i in range(7)))
    print(' Recall\t\t', *(f'|\t{mcm[i,1,1] / (mcm[i,1,1]+mcm[i,1,0]):.4f}\t' for i in range(7)))
    print(' F1-score\t', *(f'|\t{2 / (1/(mcm[i,1,1] / (mcm[i,1,1]+mcm[i,1,0])) + 1/(mcm[i,1,1] / (mcm[i,1,1]+mcm[i,0,1]))):.4f}\t' for i in range(7)))

def grad_cam(model, image, label, layer_name='block3_conv3'):
    """GradCAM method for visualizing input saliency."""
    printi('Computing GradCAM …', 0)
    # Turn label into class index
    class_idx = np.squeeze(np.argwhere(np.array(label)==1))
    # For demonstration purposes we will use the first class only
    class_idx = class_idx[0]
    print(label)
    print(class_idx)
    y_c = model.output[0, class_idx]
    conv_output = model.get_layer(layer_name).output
    grads = tf.keras.backend.gradients(y_c, conv_output)[0]
    # Normalize if necessary
    # grads = normalize(grads)
    compute_gradients = tf.keras.backend.function([model.input], [conv_output, grads])
    print(image.shape)
    output, grads_val = compute_gradients([np.expand_dims(image, axis=0)])
    output, grads_val = output[0,:], grads_val[0,:,:,:]
    print(np.unique(grads_val))

    weights = np.mean(grads_val, axis=(0, 1))
    cam = np.dot(output, weights)

    # Process CAM
    cam = cv2.resize(cam, (image.shape[0], image.shape[1]), cv2.INTER_LINEAR)
    cam = np.maximum(cam, 0)
    cam = cam / cam.max()
    printi('Succesfully computed GradCAM.', 1)
    return cam

def get_derivative(model, image, label):
    printi('Computing gradients …', 0)
    class_idx = np.squeeze(np.argwhere(np.array(label)==1))
    # For demonstration purposes we will use the first class only
    class_idx = class_idx[0]

    outp = model.output[0][class_idx]
    inp = model.input
    print(outp, inp)
    gradients = tf.keras.backend.gradients(outp, inp)
    compute_gradients = tf.keras.backend.function(inputs = [model.input],
                                                  outputs = gradients)
    # return compute_gradients([image])[0]
    gradients = compute_gradients([np.expand_dims(image, axis=0)])[0][0]

    # Print some info
    print(f'Shape of input image:\t{image.shape}')
    print(f'Range of input image:\t{np.min(image)}, {np.max(image)}')
    print(f'Label:\t\t\t{label}')
    print(f'Class index:\t\t{class_idx}')
    print(f'Shape of gradients:\t{gradients.shape}')
    print(f'# Unique values:\t{len(np.unique(gradients))}')
    printi('Computed gradients successfully.', 1)
    return gradients

def plot_saliency(model, img, lbl):
    printi('Creating saliency maps …', 0)
    # Standard Gradient Saliency
    grad = get_derivative(model, img, lbl)
    #single = Single_Saliency(mnist_model, lbl)
    #grad = single.get_grad(img)
    # filter_grad = (grad > 0.0)[...,0]
    #average_grad = single.get_average_grad(img)
    #filter_average_grad = (average_grad > 0.0).reshape((28, 28))
    plt.subplot(131)
    plt.imshow(img)
    plt.subplot(132)
    plt.imshow(grad, cmap = 'jet')
    # plt.subplot(133)
    # plt.imshow(grad*filter_grad, cmap = 'gray')
    #plt.imshow(average_grad.reshape((28, 28)), cmap = 'jet')
    #plt.imshow(average_grad.reshape((28, 28)) * filter_average_grad, cmap = 'gray')

    # GradCAM
    # gradcam = grad_cam(model, img, lbl)
    # plt.figure(figsize=(15, 10))
    # plt.subplot(131)
    # plt.title('GradCAM')
    # plt.axis('off')
    # plt.imshow(img)
    # plt.imshow(gradcam, cmap='jet', alpha=0.5)
    plt.show()

################################################################################

def getArgs(args=sys.argv[1:]):
    """
    Parses arguments from command line. Run this script with the help flag
    to see available arguments at the command line.
    """
    parser = argparse.ArgumentParser(description="Parses command.")
    parser.add_argument("--load_model",
        type=str,
        help="Load a trained model from storage.")
    parser.add_argument("--build_new_model",
        action="store_true",
        help="Build a new model.")
    parser.add_argument("--train_n_epochs",
        type=int,
        help="Train a specified model for n epochs.")
    parser.add_argument("--steps_per_epoch",
        type=int,
        help="When training, this optionally specifies the number of steps \
        per epoch.")
    parser.add_argument("--save_name",
        help="When training a model, save under this name.")
    parser.add_argument("--show_stats",
        action="store_true",
        help="Show stats about the dataset.")
    parser.add_argument("--test",
        action="store_true",
        help="Test model.")
    parser.add_argument("--plot_saliency",
        action="store_true",
        help="Compute the pixel saliency via backward gradient and \
        plot the result.")
    return parser.parse_args(args)

################################################################################

if __name__ == '__main__':

    # Get command line arguments
    configs = getArgs()

    if configs.train_n_epochs or configs.test:
        # Fix CuDNN issues with RTX cards
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        config.gpu_options.per_process_gpu_memory_fraction = 0.8
        set_session(tf.Session(config = config))

    ## HYPERPARAMS
    params = HyperParameterManager('Hyper parameters to be used globally.')
    params.N_CLASSES = 7
    params.EXTRACTION_LAYER_NUMBER = 3
    params.INPUT_SHAPE = (width, height, channels) = (130, 130, 3)
    params.MODEL_DIR   = '/mnt/e/Pretrained-Models/affordance-maps/'
    params.N_EPOCHS = 6
    params.TRAIN_TEST_SPLIT = 0.8
    params.DATA_PATH = '/mnt/e/Datasets/part-affordance-dataset-preprocessed/'
    params.RANDOM_STATE_SEED = 448

    class_dict = { 1 : 'grasp', 2 : 'cut', 3 : 'scoop', 4 : 'contain',
                   5 : 'pound', 6 : 'support', 7 : 'wrap-grasp' }

    # Build training and validation data generators from the preprocessed
    # affordance dataset
    printi('Building data generators …', 0)
    train_gen, val_gen, test_gen = datagen.build_generators(
        params.DATA_PATH, params.TRAIN_TEST_SPLIT, class_dict)
    printi('Successfully built data generators with train and validation split '
           f'of {params.TRAIN_TEST_SPLIT}', 1)

    print(train_gen[0][1][0])

    # Load a pretrained model from storage
    if configs.load_model:
        printi('Loading pretrained model …', 0)
        model = load_model(params.MODEL_DIR, configs.load_model)
        printi('Loaded pretrained model successfully.', 1)

    # Build a new model
    if configs.build_new_model:
        printi('Building new model …', 0)
        model = build_new_model()
        printi('Built new model.', 1)

    # Train the model
    if configs.train_n_epochs:
        params.N_EPOCHS = configs.train_n_epochs

        printi('Fitting model to data …', 0)
        history = model.fit_generator(generator=train_gen,
                                      steps_per_epoch=configs.steps_per_epoch,
                                      validation_data=test_gen,
                                      validation_steps=configs.steps_per_epoch,
                                      epochs=params.N_EPOCHS)
        printi('Fitted model to data.', 1)

        printi('Saving model to storage …', 0)
        last_loss = history.history['val_loss'][-1]
        save_name = save_model(model = model,
                               dir = params.MODEL_DIR,
                               n_epochs = params.N_EPOCHS,
                               last_loss = f'{last_loss:.4f}',
                               save_name = configs.save_name,
                               history = history)
        printi(f"Saved model at '{params.MODEL_DIR}{save_name}.h5'", 1)

        printi('Plotting training history …', 0)
        plot_history(history,
                     also_print=True,
                     save_to_storage=f'{params.MODEL_DIR}/histories/{save_name}'
                                     '.historyplot.png')
        printi('Plots closed.', 1)

    # Output some stats about the dataset
    if configs.show_stats:
        printi('Showing dataset statistics …', 0)
        show_data_stats()
        printi('', 1)

    # Test given model on validation data
    if configs.test:
        printi('Testing model …', 0)
        test_model()
        printi('Finished tests.', 1)

    if configs.plot_saliency:
        batch_x, batch_y = train_gen.__getitem__(index=100)
        image, label = batch_x[0], batch_y[0]
        plot_saliency(model, image, label)

    printi('End of script. Exiting.', 1)
