###############################################################################
# Keras Data Generator
# Code inspired by https://github.com/afshinea/keras-data-generator
###############################################################################

import numpy as np
import pandas as pd
import tensorflow.compat.v1 as tf
from tensorflow.compat.v1.keras.utils import Sequence
from tensorflow.compat.v1.keras.preprocessing.image import ImageDataGenerator
import os, random
from skimage.io import imread
from skimage.transform import resize
from scipy.io import loadmat
from sklearn.preprocessing import MultiLabelBinarizer

def build_generators(data_path, train_test_split, class_dict):
    '''
    Build training, validation and testing data generators with Keras'
    ImageDataGenerator.
    The path that is given to this function must include two subdirectories
    'training/' and 'validation/', else an error is thrown.
    The training dir is used to create a training and validation generator
    with <train_test_split>, and the validation dir is used for a test
    generator.
    ---
    @params
    data_path : path to dataset as string
    train_test_split : percentage of train data to be used for validation
    ---
    @returns
    train_generator, valid_generator, test_generator : iterables that can
    be used for fitting a Keras model with model.fit(<generator>)
    '''

    def create_dataframe(path):
        items = sorted(os.listdir(path))
        xs, ys = [], []
        for i in np.arange(0, len(items), 2):
            # The second item is always the .jpg image, so take this as x
            xs.append(items[i+1])
            # The first one is the label which needs to be read from matlab
            y = loadmat(path+items[i])['gt_label']
            y = np.unique(y) # Get unique values
            y = y[np.nonzero(y)] # Remove 0 label
            y = [class_dict[label_number] for label_number in y]
            ys.append(y)
            # binary_y = np.zeros(7) # Create binary 1-D array of size 7
            # binary_y[y-1] = 1 # Populate with 1 at index of class
            # ys.append(list(binary_y))
        return pd.DataFrame({'X':xs, 'y':ys})

    train_dir = data_path + 'training/'
    test_dir = data_path + 'validation/'
    train_df = create_dataframe(train_dir)
    test_df = create_dataframe(test_dir)

    # Build data generators with augmentation params
    data_aug_params = dict(
        rescale=1./255,
        # featurewise_center=True,
        # featurewise_std_normalization=True,
        rotation_range=90,
        width_shift_range=0.1,
        height_shift_range=0.1,
        zoom_range=0.2,
        shear_range=0.2,
        horizontal_flip=True)
    train_datagen = ImageDataGenerator(**data_aug_params,
                                    validation_split=0.15)
    test_datagen = ImageDataGenerator(rescale=1./255.)

    train_generator = train_datagen.flow_from_dataframe(
        train_df,
        directory = train_dir,
        x_col = 'X',
        y_col = 'y',
        subset = 'training',
        target_size = (130, 130),
        # seed = 42,
        # shuffle = True,
        batch_size = 32,
        class_mode = 'categorical')
    valid_generator = train_datagen.flow_from_dataframe(
        train_df,
        directory = train_dir,
        x_col = 'X',
        y_col = 'y',
        subset = 'validation',
        target_size = (130, 130),
        # seed = 42,
        # shuffle = True,
        batch_size = 32,
        class_mode = 'categorical')
    test_generator = test_datagen.flow_from_dataframe(
        test_df,
        directory = test_dir,
        x_col = 'X',
        y_col = 'y',
        target_size=(130, 130),
        batch_size=32,
        class_mode='categorical')

    return train_generator, valid_generator, test_generator

class DataGenerator(Sequence):
    '''
    Generator for multi label classification.
    '''
    def __init__(self, x_ids, labels, path, input_shape=(32,32), n_channels=1,
                 n_classes=10, batch_size=32, shuffle=True,
                 x_load_func=None, y_load_func=None):
        if (x_load_func==None) or (y_load_func==None):
            raise ValueError('Functions for loading X and Y from storage must be specified.')
        self.x_ids = x_ids
        self.labels = labels
        self.path = path
        if self.path[-1] != '/':
            self.path.append('/') # Append slash to path if not present
        self.input_shape = input_shape
        self.n_channels = n_channels
        self.n_classes = n_classes
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.x_load_func = x_load_func
        self.y_load_func = y_load_func
        self.on_epoch_end()

    def __len__(self):
        '''
        The length of the data generator, i.e. the number of batches that can
        be generated computed by dividing the number of data instances through
        the specified batch size.
        ---
        @returns the number of batches / length of the data generator
        '''
        return int(np.floor(len(self.x_ids) / self.batch_size))

    def __getitem__(self, index):
        '''
        Generates one batch of data of size <self.batch_size>
        ---
        @params
        index : index of the batch to generate
        ---
        @returns
        X : the generated batch of input data of size
            (batch_size, *input_shape, n_channels)
        y : the generated batch of labels corresponding to the inputs
        '''
        # Generate indexes of the batch
        indexes = self.indexes[index*self.batch_size:(index+1)*self.batch_size]
        # Find list of IDs
        temp_ids = [self.x_ids[k] for k in indexes]
        # Generate data
        X, y = self.__data_generation(temp_ids)

        return X, y

    def on_epoch_end(self):
        '''
        Updates internal indexes after each epoch.
        '''
        self.indexes = np.arange(len(self.x_ids))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, temp_ids):
        '''
        Generates data containing <self.batch_size> samples
        ---
        @params
        temp_ids :
        '''
        # X : (n_samples, *dim, n_channels)
        # Initialization
        X = np.empty((self.batch_size, *self.input_shape, self.n_channels))
        y = [] #np.empty((self.batch_size), dtype=int)

        # Generate data
        for i, ID in enumerate(temp_ids):
            # Store sample
            X[i,] = self.x_load_func(self.path+ID, self.input_shape)
            # Store class
            y.append(self.y_load_func(self.path+self.labels[ID]))

        # Binarize labels.
        # For single label classification, use:
        # y = keras.utils.to_categorical(y, num_classes=self.n_classes)
        # For multi label classification, use:
        possible_labels = range(1, self.n_classes + 1)
        y = MultiLabelBinarizer(possible_labels).fit_transform(y)

        return X, y

def build_sequential_generators(data_path, train_test_split, class_dict):
    '''
    Build a training and validation data generator from the preprocessed
    affordance dataset.
    ---
    @params
    data_path : absolute path to the dataset
    train_test_split :
    '''
    # Get sorted filenames from storage
    # filenames = sorted(os.listdir(params.DATA_PATH))

    train_dir = data_path + 'training/'
    test_dir = data_path + 'validation/'

    train_items = sorted(os.listdir(train_dir))
    test_items  = sorted(os.listdir(test_dir))

    # Create a list holding all filepaths to the inputs, and a dict mapping
    # these paths to the paths of corresponding labels
    def map_labels(items):
        x_ids  = []
        labels = dict()
        for i in np.arange(0,len(items),2):
            labels[items[i+1]] = items[i]
            x_ids.append(items[i+1])
        return x_ids, labels

    train_ids, train_labels = map_labels(train_items)
    test_ids, test_labels = map_labels(test_items)

    # Shuffle the ids
    # random.seed(params.RANDOM_STATE_SEED)
    # random.shuffle(train_ids)

    # Create seperate train and validation ids
    # assert 0 <= params.TRAIN_TEST_SPLIT <= 1, "Train test split must be between 0 and 1."
    # split_index = int(len(x_ids)*params.TRAIN_TEST_SPLIT)
    # train_ids = x_ids[:split_index]
    # val_ids   = x_ids[split_index:]

    # Functions for the DataGenerator how files should be read from storage given
    # the full file path
    def load_x(filepath, input_shape):
        return resize(imread(filepath), input_shape)

    def load_y(filepath):
        '''Gets the 3-D input of pixel-wise labels as input and returns the
        unique elements without 0'''
        y = loadmat(filepath)['gt_label']
        y = np.unique(y) # Get unique values
        y = y[np.nonzero(y)] # Remove 0 label
        return y

    datagen_params = dict(input_shape = (130, 130),
        n_channels = 3, n_classes = 7, batch_size = 64, shuffle = False,
        x_load_func = load_x, y_load_func = load_y)

    train_gen = DataGenerator(
        x_ids = train_ids,
        labels = train_labels,
        path = train_dir,
        **datagen_params)
    test_gen  = DataGenerator(
        x_ids = test_ids,
        labels = test_labels,
        path = test_dir,
        **datagen_params)
    return train_gen, None, test_gen
