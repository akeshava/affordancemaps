Model: "model_1"
_________________________________________________________________
Layer (type)                 Output Shape              Param #
=================================================================
input_1 (InputLayer)         (None, 130, 130, 3)       0
_________________________________________________________________
block1_conv1 (Conv2D)        (None, 130, 130, 64)      1792
_________________________________________________________________
block1_conv2 (Conv2D)        (None, 130, 130, 64)      36928
_________________________________________________________________
block1_pool (MaxPooling2D)   (None, 65, 65, 64)        0
_________________________________________________________________
block2_conv1 (Conv2D)        (None, 65, 65, 128)       73856
_________________________________________________________________
block2_conv2 (Conv2D)        (None, 65, 65, 128)       147584
_________________________________________________________________
block2_pool (MaxPooling2D)   (None, 32, 32, 128)       0
_________________________________________________________________
block3_conv1 (Conv2D)        (None, 32, 32, 256)       295168
_________________________________________________________________
block3_conv2 (Conv2D)        (None, 32, 32, 256)       590080
_________________________________________________________________
block3_conv3 (Conv2D)        (None, 32, 32, 256)       590080
_________________________________________________________________
block3_pool (MaxPooling2D)   (None, 16, 16, 256)       0
_________________________________________________________________
conv2d_1 (Conv2D)            (None, 14, 14, 64)        147520
_________________________________________________________________
max_pooling2d_1 (MaxPooling2 (None, 7, 7, 64)          0
_________________________________________________________________
flatten_1 (Flatten)          (None, 3136)              0
_________________________________________________________________
dense_1 (Dense)              (None, 256)               803072
_________________________________________________________________
dropout_1 (Dropout)          (None, 256)               0
_________________________________________________________________
dense_2 (Dense)              (None, 7)                 1799
=================================================================
Total params: 2,687,879
Trainable params: 952,391
Non-trainable params: 1,735,488
_________________________________________________________________
WARNING:tensorflow:From /home/pscl/Development/affordancemaps/tfenv/lib/python3.6/site-packages/keras/backend/tensorflow_backend.py:422: The name tf.global_variables is deprecated. Please use tf.compat.v1.global_variables instead.

Epoch 1/6
2019-11-25 21:22:09.622960: I tensorflow/stream_executor/platform/default/dso_loader.cc:42] Successfully opened dynamic library libcublas.so.10.0
2019-11-25 21:22:10.098210: I tensorflow/stream_executor/platform/default/dso_loader.cc:42] Successfully opened dynamic library libcudnn.so.7
124/124 [==============================] - 35s 282ms/step - loss: 0.5389 - precision: 0.6928 - recall: 0.6821 - val_loss: 0.1610 - val_precision: 0.8259 - val_recall: 0.7668
Epoch 2/6
124/124 [==============================] - 28s 226ms/step - loss: 0.1261 - precision: 0.8618 - recall: 0.8049 - val_loss: 0.1006 - val_precision: 0.8862 - val_recall: 0.8350
Epoch 3/6
124/124 [==============================] - 29s 230ms/step - loss: 0.0684 - precision: 0.9025 - recall: 0.8572 - val_loss: 0.0671 - val_precision: 0.9149 - val_recall: 0.8753
Epoch 4/6
124/124 [==============================] - 28s 226ms/step - loss: 0.0448 - precision: 0.9239 - recall: 0.8888 - val_loss: 0.0593 - val_precision: 0.9313 - val_recall: 0.9000
Epoch 5/6
124/124 [==============================] - 27s 215ms/step - loss: 0.0286 - precision: 0.9373 - recall: 0.9092 - val_loss: 0.0483 - val_precision: 0.9423 - val_recall: 0.9170
Epoch 6/6
124/124 [==============================] - 27s 217ms/step - loss: 0.0228 - precision: 0.9465 - recall: 0.9235 - val_loss: 0.0498 - val_precision: 0.9502 - val_recall: 0.9290
[0.04981829226016998, 0.9516118764877319, 0.9309849739074707]
